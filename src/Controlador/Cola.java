/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

/**
 *
 * @author LENOVO
 */
public class Cola <E> extends ListaEnlazada<E>{
    private Integer tope;

    public Cola(Integer Tope) {
        this.tope = Tope;
    }
    
    public Boolean estaLleno(){
        if (tope==0) 
            return false;
        else if (tope == getSize().intValue()) 
            return true;
        else
            return false;
    }
    
    public void queue (E dato){
        if (!estaLleno()) {
            insertar(dato, getSize()-1);
        }else{
            System.out.println("La pila esta Llena");
        }
    }
    
    public E dequeue(Integer pos){
        E auxdato = null;
        if (!estaVacia()) {
            if (pos >= 0 && pos < getSize()) {
                for (int i = 0; i <= pos; i++) {
                    auxdato = eliminarDato(0);
                }
                return auxdato;
        }else{
            System.out.println("La pila esta Vacia");
        }
    }else{
            System.out.println("La pila esta Vacia");
        }
    }
    
    public Integer getTope(){
        return tope;
    }
}
