/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Tablas;

import Controlador.Cola;
import Modelo.Prestamos;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author LENOVO
 */
public class TablaPrestamos extends AbstractTableModel{
    private Cola<Prestamos> cola;

    public Cola<Prestamos> getCola() {
        return cola;
    }

    public void setCola(Cola<Prestamos> cola) {
        this.cola = cola;
    }
    
    @Override
    public int getRowCount() {
        return cola.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }
    
    @Override
    public String getColumnName(int column) {
        switch(column) {
            case 0: return "Ticket";
            default: return null;
        }
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Prestamos prestamos = (Prestamos) cola.obtenerDato(rowIndex);
        switch(columnIndex) {
            case 0: return prestamos.getTicket();
            default: return null;
        }
    }
}
